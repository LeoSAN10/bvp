const express = require('express');
const router = express.Router();

/* GET users listing. */
router.get('/best', (req, res) => {
  res.send([
  	 {name: "name9020", time: "Very good"},
  	 {name: "Leonardo", time: "Very bad"},
  	 {name: "Mike", time: "So so"}]);
});

module.exports = router;
