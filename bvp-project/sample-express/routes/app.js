const express = require('express');
const router = express.Router();
const faker = require('faker');

/* GET opinion page. */
router.get('/opinion', (req, res) => {
   var cities = [ 
       { "name": "Like", "id": "Like"}, 
  	   { "name": "Dislike", "id": "Dislike" },
       { "name": "add opinion", "id": "add opinion"}
  	];

    res.render('opinion', 
  			{ "opinion": cities}
        );
});

router.get('/comm', (req, res) => {
    res.render('comm', 
        { "comm": [ 
           { "name": "Eugene Matyuto (Cowek), Guslar", "id": "Guslar"}, 
           { "name": "Eugene Sosyura (Mutus), Airport", "id": "Airport" },
           { "name": "Deih, Yarila", "id": "Yarila" }

          ]
        }
    );
});


router.get('/foto', (req, res) => {
    res.render('foto', 
        { "foto": [ 
           { "name": "Guslar, October street, 16,04.04.2016", "id": "Guslar", "url":"https://rdnv.me/images/2015/12/streetart_oktyabrskaya_16-2.jpg"}, 
           { "name": "Airport, ул. Аэродромная, 4/3, 05.10.2015", "id": "Airport", "url": "http://www.ctv.by/sites/default/files/field2/art_6.jpg"},
           { "name": "Yarila, ул. Рабкоровская, 13, 20.05.2016", "id": "Yarila","url": "https://rdnv.me/images/2016/01/streetart_rabkorovskaya_13.jpg"}
          ]
        }
    );
});

module.exports = router;
